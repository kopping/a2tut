import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routes } from './app.routes';

import { MySharedModule } from './shared/my-shared.module';
import {ExampleAComponent, ExampleBComponent} from "./example/index";
import {CarListComponent} from "./example/car-list.component";


import {DataListModule,DataTableModule,SharedModule,CheckboxModule} from 'primeng/primeng';
import {ExampleCComponent} from "./example/example-c.component";
import {IOComponent} from "./example/child.component";
import {ParentComponent} from "./example/parent.component";

@NgModule({
  imports: [CheckboxModule,DataListModule,DataTableModule,SharedModule, BrowserModule, HttpModule, RouterModule.forRoot(routes), MySharedModule.forRoot(),CommonModule],
  declarations: [AppComponent, ExampleAComponent, ExampleBComponent, ExampleCComponent, ParentComponent, CarListComponent, IOComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  }],
  bootstrap: [AppComponent]

})

export class AppModule { }
