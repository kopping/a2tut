/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './example-a.component';
export * from './example-b.component';
export * from './example-c.component';
export * from './car-list.component';
export * from './child.component';
export * from './example.routes';
