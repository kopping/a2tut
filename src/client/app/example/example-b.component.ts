import {Component, OnInit} from '@angular/core';
import {CarService} from "../shared/vehicle-service-api/car.service";
import {Car} from "../shared/vehicle-service-api/models/car.model";

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-example-b',
  templateUrl: 'example-b.component.html',
  styleUrls: ['example.component.css']
})
export class ExampleBComponent implements OnInit {
  cars : Car[] = [];
  errorMessage: string;

  constructor(public carService: CarService) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {
    this.getCars();
  }

  /**
   * Handle the nameListService observable
   */
  getCars() {
    this.carService.get()
      .subscribe(
        names => this.cars = names,
        error =>  this.errorMessage = <any>error
      );
  }

  doit(mesg:string){
    alert(mesg)
  }


}
