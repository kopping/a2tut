import {Component, Input, EventEmitter, Output, HostListener} from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'child',
  templateUrl: 'child.component.html',
  styleUrls: ['example.component.css']
})
export class IOComponent{
  @Input()  color: string;

  @Output() onEmitColor = new EventEmitter<string>();


  @HostListener('mouseover')
  onMouseover() {
    this.emitMessage();
  }

  emitMessage() {
    this.onEmitColor.emit(this.color);
  }
}
