import {Component, Input} from '@angular/core';
import {Car} from "../shared/vehicle-service-api/models/car.model";

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'car-list',
  templateUrl: 'car-list.component.html',
  styleUrls: ['example.component.css']
})
export class CarListComponent{
  @Input() cars: Car[];
}
