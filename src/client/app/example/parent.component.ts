import { Component } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-example-d',
  templateUrl: 'parent.component.html',
  styleUrls: ['example.component.css']
})
export class ParentComponent {

  target_color : string;

  setTargetColor( target_color:string){
    this.target_color = target_color;
  }

}
