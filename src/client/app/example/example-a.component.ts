import { Component } from '@angular/core';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-example-a',
  templateUrl: 'example-a.component.html',
  styleUrls: ['example.component.css']
})
export class ExampleAComponent {
  cars : any[];

  constructor() {
    this.cars = [];
    this.cars.push({regnr: "AAA111", brand:"Volvo", price:"120000"});
    this.cars.push({regnr: "BBB222", brand:"BMW", price:"460000"});
    this.cars.push({regnr: "CCC333", brand:"Fiat", price:"23000"});
  }


}
