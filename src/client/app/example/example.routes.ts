import { Route } from '@angular/router';
import {ExampleAComponent, ExampleBComponent, ExampleCComponent} from "./index";
import {ParentComponent} from "./parent.component";

export const ExampleRoutes: Route[] = [
  {
    path: '',
    component: ExampleAComponent
  },
  {
    path: 'example-a',
    component: ExampleAComponent
  },
  {
    path: 'example-b',
    component: ExampleBComponent
  },
  {
    path: 'example-c',
    component: ExampleCComponent
  },
  {
    path: 'example-d',
    component: ParentComponent
  }
];
