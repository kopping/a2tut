import { Routes } from '@angular/router';

import { ExampleRoutes } from './example/index';

export const routes: Routes = [
  ...ExampleRoutes
];
