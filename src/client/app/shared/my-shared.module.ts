import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {ToolbarComponent} from './toolbar/index';
import {NavbarComponent} from './navbar/index';
import {Highlighter1Directive} from "./directives/highlight1.directive";
import {Highlighter2Directive} from "./directives/highlight2.directive";
import {Highlighter3Directive} from "./directives/highlight3.directive";
import {CarService} from "./vehicle-service-api/car.service";
import {MotorcycleService} from "./vehicle-service-api/motorcycle.service";
import {Highlighter4Directive} from "./directives/highlight4.directive";
import {SalePipe} from "./pipes/sale.pipe";

/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [ToolbarComponent, NavbarComponent, Highlighter1Directive, Highlighter2Directive, Highlighter3Directive, Highlighter4Directive, SalePipe],
  exports: [ToolbarComponent, NavbarComponent, Highlighter1Directive, Highlighter2Directive, Highlighter3Directive, Highlighter4Directive, SalePipe,
    CommonModule, FormsModule, RouterModule]
})
export class MySharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MySharedModule,
      providers: [CarService, MotorcycleService]
    };
  }
}
