/**
 * Created by xxkoppia on 2016-09-26.
 */
export class Car {
  regnr: string;
  brand: string;
  price: number;
}
