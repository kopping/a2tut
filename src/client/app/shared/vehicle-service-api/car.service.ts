/**
 * Created by xxkoppia on 2016-09-26.
 */
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Car} from "./models/car.model";

@Injectable()
export class CarService {
  cars: Car[] = [];

  // private serviceUrl = env.VEHICLE_API_URL;
  private serviceUrl: string = '';
  // private checksum: string = '';
  private pendingRequest: Observable<Car[]>;

  constructor(private http: Http) {
  }

  get(refresh = false): Observable<Car[]> {

    if (refresh) {
      this.clearCache();
    } else {

      if (this.cars.length > 0) {
        return Observable.from([this.cars]);
      }
      /** Caching strategies**/

      /**
       *  Strategy for enormous amounts of data:
       *  get current checksum for cars and compare to this.checksum
       *  if( different) this.clearCache();
       */

      /** Strategy for fixed amount of data like profile data or data related to the JWT used to authorize
       *  ie. the JWT persist in the browser between page reloads,
       *  then we would expect the profile to persist the same way
       * if( found in localStorage ){
       *    return Observable.from([localStorage("cars")]);
       * }
       */
    }

    if (!this.pendingRequest) {  // don't do more than one request at a time.

      this.pendingRequest = this.http.get(this.serviceUrl + './mockupdata/cars.json ')
        .map((response: Response) => response.json())
        .map((data: Car[]) => {
          return this.cars = data;
        })
        .catch(this.handleError)
        .publishReplay(1).refCount();

    }

    return this.pendingRequest;
  }

  clearCache()  {
    this.cars = null;
    this.pendingRequest = null;

  }

  handleError(error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error.message || 'Server error: ' + error.status + ' ' + error.statusText;
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
