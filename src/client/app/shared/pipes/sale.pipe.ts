import { Pipe, PipeTransform } from '@angular/core';
/*
 *    sale% av priset
 */
@Pipe({name: 'sale'})
export class SalePipe implements PipeTransform {
  transform(price: number, sale: number): number {
    return price * sale /100 ;
  }
}
