import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({ selector: '[highlighter1]' })

export class Highlighter1Directive {
  constructor(el: ElementRef, renderer: Renderer) {
    renderer.setElementStyle( el.nativeElement,
                              'backgroundColor',
                              'yellow');
  }
}

