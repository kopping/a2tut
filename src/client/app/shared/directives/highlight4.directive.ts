
import {Directive, ElementRef, Input, Renderer, SimpleChange, OnChanges} from '@angular/core';

@Directive({ selector: '[dynamic-highlight]' })
export class Highlighter4Directive  implements OnChanges {
  constructor(private el: ElementRef, private renderer: Renderer) { }

  @Input('color') the_color: string;

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}) {
    for (let propName in changes) {
      let chng = changes[propName];
      let cur = JSON.stringify(chng.currentValue);
      let prev = JSON.stringify(chng.previousValue);
      console.log(cur, prev);
      this.highlight();
    }
  }

  private highlight() {
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', this.the_color);

  }
}
