
import { Directive, ElementRef, HostListener, Input, Renderer } from '@angular/core';

@Directive({ selector: '[highlighter3]' })
export class Highlighter3Directive {
  constructor(private el: ElementRef, private renderer: Renderer) { }

  @Input('color') the_color: string;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.highlight(this.the_color);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlight(null);
  }


  private highlight(the_color: string) {
    this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', the_color);
  }
}
